package polimorfisme;

public class Lingkaran extends BangunDatar {
    private int radius;
    private static final double Pi = 3.14;

    public Lingkaran(int radius){
        this.radius = radius;
    }

    @Override
    public void luas(){
        System.out.println("Luas Lingkaran: " + (Pi * radius * radius));
    }

    public void keliling(){
        System.out.println("Keliling Lingkaran : " + (2 * Pi * radius));
    }
}
