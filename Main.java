package polimorfisme;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner inp = new Scanner(System.in);

        System.out.println("Masukkan Sisi Persegi : ");
        int sisi = inp.nextInt();
        Persegi bangun1 = new Persegi(sisi);
        bangun1.luas();
        bangun1.keliling();
        System.out.println("\n");

        System.out.println("Masukkan jari - jari Lingkaran : ");
        int radius = inp.nextInt();
        Lingkaran bangun2 = new Lingkaran(radius);
        bangun2.luas();
        bangun2.keliling();
        System.out.println("\n");

        System.out.println("Masukkan panjang Persegi Panjang : ");
        int panjang = inp.nextInt();
        System.out.println("Masukkan lebar Persegi Panjang : ");
        int lebar = inp.nextInt();
        PersegiPanjang bangun3 = new PersegiPanjang(panjang, lebar);
        bangun3.luas();
        bangun3.keliling();
        System.out.println("\n"); 
        
        System.out.println("Masukkan Alas Segitiga : ");
        int alas = inp.nextInt();
        System.out.println("Masukkan Tinggi Segitiga : ");
        int tinggi = inp.nextInt();
        Segitiga bangun4 = new Segitiga(alas, tinggi);
        bangun4.luas();
        bangun4.keliling();

        inp.close();
    }
}
